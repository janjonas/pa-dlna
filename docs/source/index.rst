.. pa-dlna documentation master file, created by
   sphinx-quickstart on Tue Dec  6 10:56:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pa-dlna |version|
=================

.. image:: _static/coverage.svg
   :alt: pa-dlna test coverage

.. include:: ../../README.rst

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Table of Contents

   ReadMe <self>
   usage
   configuration
   Default Configuration <default-config>
   upnp-cmd
   pa-dlna
   development
   history
   Repository <https://gitlab.com/xdegaye/pa-dlna>
