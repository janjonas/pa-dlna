"""Pulseaudio prototypes.

This file is generated by tools/parse_libpulse.py.
DO NOT MODIFY.
"""

# Prototypes.
PROTOTYPES = {'pa_context_connect': ('int',
                        ['pa_context *',
                         'char *',
                         'pa_context_flags_t',
                         'pa_spawn_api *']),
 'pa_context_disconnect': ('void', ['pa_context *']),
 'pa_context_errno': ('int', ['pa_context *']),
 'pa_context_get_protocol_version': ('uint32_t', ['pa_context *']),
 'pa_context_get_server': ('char *', ['pa_context *']),
 'pa_context_get_server_info': ('pa_operation *',
                                ['pa_context *',
                                 'pa_server_info_cb_t',
                                 'void *']),
 'pa_context_get_server_protocol_version': ('uint32_t', ['pa_context *']),
 'pa_context_get_sink_info_by_name': ('pa_operation *',
                                      ['pa_context *',
                                       'char *',
                                       'pa_sink_info_cb_t',
                                       'void *']),
 'pa_context_get_sink_info_list': ('pa_operation *',
                                   ['pa_context *',
                                    'pa_sink_info_cb_t',
                                    'void *']),
 'pa_context_get_sink_input_info_list': ('pa_operation *',
                                         ['pa_context *',
                                          'pa_sink_input_info_cb_t',
                                          'void *']),
 'pa_context_get_state': ('pa_context_state_t', ['pa_context *']),
 'pa_context_load_module': ('pa_operation *',
                            ['pa_context *',
                             'char *',
                             'char *',
                             'pa_context_index_cb_t',
                             'void *']),
 'pa_context_new': ('pa_context *', ['pa_mainloop_api *', 'char *']),
 'pa_context_ref': ('pa_context *', ['pa_context *']),
 'pa_context_set_state_callback': ('void',
                                   ['pa_context *',
                                    'pa_context_notify_cb_t',
                                    'void *']),
 'pa_context_set_subscribe_callback': ('void',
                                       ['pa_context *',
                                        'pa_context_subscribe_cb_t',
                                        'void *']),
 'pa_context_subscribe': ('pa_operation *',
                          ['pa_context *',
                           'pa_subscription_mask_t',
                           'pa_context_success_cb_t',
                           'void *']),
 'pa_context_unload_module': ('pa_operation *',
                              ['pa_context *',
                               'uint32_t',
                               'pa_context_success_cb_t',
                               'void *']),
 'pa_context_unref': ('void', ['pa_context *']),
 'pa_operation_cancel': ('void', ['pa_operation *']),
 'pa_operation_ref': ('pa_operation *', ['pa_operation *']),
 'pa_operation_unref': ('void', ['pa_operation *']),
 'pa_proplist_gets': ('char *', ['pa_proplist *', 'char *']),
 'pa_proplist_iterate': ('char *', ['pa_proplist *', 'void **']),
 'pa_strerror': ('char *', ['int'])}

# Callbacks.
CALLBACKS = {'defer_enable': ('void', ['pa_defer_event *', 'int']),
 'defer_free': ('void', ['pa_defer_event *']),
 'defer_new': ('pa_defer_event *',
               ['pa_mainloop_api *', 'pa_defer_event_cb_t', 'void *']),
 'defer_set_destroy': ('void',
                       ['pa_defer_event *', 'pa_defer_event_destroy_cb_t']),
 'io_enable': ('void', ['pa_io_event *', 'pa_io_event_flags_t']),
 'io_free': ('void', ['pa_io_event *']),
 'io_new': ('pa_io_event *',
            ['pa_mainloop_api *',
             'int',
             'pa_io_event_flags_t',
             'pa_io_event_cb_t',
             'void *']),
 'io_set_destroy': ('void', ['pa_io_event *', 'pa_io_event_destroy_cb_t']),
 'pa_context_index_cb_t': ('void', ['pa_context *', 'uint32_t', 'void *']),
 'pa_context_notify_cb_t': ('void', ['pa_context *', 'void *']),
 'pa_context_subscribe_cb_t': ('void',
                               ['pa_context *',
                                'pa_subscription_event_type_t',
                                'uint32_t',
                                'void *']),
 'pa_context_success_cb_t': ('void', ['pa_context *', 'int', 'void *']),
 'pa_defer_event_cb_t': ('void',
                         ['pa_mainloop_api *', 'pa_defer_event *', 'void *']),
 'pa_defer_event_destroy_cb_t': ('void',
                                 ['pa_mainloop_api *',
                                  'pa_defer_event *',
                                  'void *']),
 'pa_io_event_cb_t': ('void',
                      ['pa_mainloop_api *',
                       'pa_io_event *',
                       'int',
                       'pa_io_event_flags_t',
                       'void *']),
 'pa_io_event_destroy_cb_t': ('void',
                              ['pa_mainloop_api *', 'pa_io_event *', 'void *']),
 'pa_server_info_cb_t': ('void',
                         ['pa_context *', 'pa_server_info *', 'void *']),
 'pa_sink_info_cb_t': ('void',
                       ['pa_context *', 'pa_sink_info *', 'int', 'void *']),
 'pa_sink_input_info_cb_t': ('void',
                             ['pa_context *',
                              'pa_sink_input_info *',
                              'int',
                              'void *']),
 'pa_time_event_cb_t': ('void',
                        ['pa_mainloop_api *',
                         'pa_time_event *',
                         'struct timeval *',
                         'void *']),
 'pa_time_event_destroy_cb_t': ('void',
                                ['pa_mainloop_api *',
                                 'pa_time_event *',
                                 'void *']),
 'quit': ('void', ['pa_mainloop_api *', 'int']),
 'time_free': ('void', ['pa_time_event *']),
 'time_new': ('pa_time_event *',
              ['pa_mainloop_api *',
               'struct timeval *',
               'pa_time_event_cb_t',
               'void *']),
 'time_restart': ('void', ['pa_time_event *', 'struct timeval *']),
 'time_set_destroy': ('void',
                      ['pa_time_event *', 'pa_time_event_destroy_cb_t'])}
