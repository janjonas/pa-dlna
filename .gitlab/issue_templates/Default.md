#### Bug report.

<!--
    A clear and concise description of what the bug is.
-->

### Your environment.

- Pulseaudio version:
- Pipewire version:
- pa-dlna version:
- DLNA device name:
- Selected encoder:
- Network type (wired, wifi):

### Steps to reproduce.

### Relevant logs or configuration.

<!--
    Please use code blocks (```) to paste logs, code or pa-dlna.conf as it's
    tough to read otherwise.
    Please try to paste only the relevant part of the logs.
-->

<!--
    You can freely edit this text. Remove any lines you believe are unnecessary.
-->
